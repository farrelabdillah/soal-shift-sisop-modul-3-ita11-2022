1. Novak adalah seorang mahasiswa biasa yang terlalu sering berselancar di internet. Pada suatu saat, Ia menemukan sebuah informasi bahwa ada suatu situs yang tidak memiliki pengguna. Ia mendownload berbagai informasi yang ada dari situs tersebut dan menemukan sebuah file dengan tulisan yang tidak jelas. Setelah diperhatikan lagi, kode tersebut berformat base64. Ia lalu meminta kepada anda untuk membuat program untuk memecahkan kode-kode di dalam file yang Ia simpan di drive dengan cara decoding dengan base 64. Agar lebih cepat, Ia sarankan untuk menggunakan thread.

A. Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan nama quote untuk file zip quote.zip dan music untuk file zip music.zip. Unzip ini dilakukan dengan bersamaan menggunakan thread.

B. Decode semua file .txt yang ada dengan base 64 dan masukkan hasilnya dalam satu file .txt yang baru untuk masing-masing folder (Hasilnya nanti ada dua file .txt) pada saat yang sama dengan menggunakan thread dan dengan nama quote.txt dan music.txt. Masing-masing kalimat dipisahkan dengan newline/enter.

C. Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama hasil.

D. Folder hasil di-zip menjadi file hasil.zip dengan password 'mihinomenest[Nama user]'. (contoh password : mihinomenestnovak)

E. Karena ada yang kurang, kalian diminta untuk unzip file hasil.zip dan buat file no.txt dengan tulisan 'No' pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt menjadi hasil.zip, dengan password yang sama seperti sebelumnya.

Catatan :
Boleh menggunakan execv(), tapi bukan mkdir, atau system
Jika ada pembenaran soal, akan di-update di catatan
Kedua file .zip berada di folder modul
Nama user di passwordnya adalah nama salah satu anggota kelompok, baik yang mengerjakan soal tersebut atau tidak. Misalkan satu kelompok memiliki anggota namanya Yudhistira, Werkudara, dan Janaka. Yang mengerjakan adalah Janaka. Nama passwordnya bisa mihinomenestyudhistira, mihinomenestwerkudara, atau mihinomenestjanaka
Menggunakan apa yang sudah dipelajari di Modul 3 dan, kalau  perlu, di Modul 1 dan 2

2. Bluemary adalah seorang Top Global 1 di salah satu platform online judge. Suatu hari Ia ingin membuat online judge nya sendiri, namun dikarenakan Ia sibuk untuk mempertahankan top global nya, maka Ia meminta kamu untuk membantunya dalam membuat online judge sederhana. Online judge sederhana akan dibuat dengan sistem client-server dengan beberapa kriteria sebagai berikut:

A. Pada saat client terhubung ke server, terdapat dua pilihan pertama yaitu register dan login. Jika memilih register, client akan diminta input id dan passwordnya untuk dikirimkan ke server. Data input akan disimpan ke file users.txt dengan format username:password. Jika client memilih login, server juga akan meminta client untuk input id dan passwordnya lalu server akan mencari data di users.txt yang sesuai dengan input client. Jika data yang sesuai ditemukan, maka client dapat login dan dapat menggunakan command-command yang ada pada sistem. Jika tidak maka server akan menolak login client. Username dan password memiliki kriteria sebagai berikut:
Username unique (tidak boleh ada user yang memiliki username yang sama)
Password minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil
Format users.txt:

users.txt
username:password
username2:password2


B. Sistem memiliki sebuah database pada server untuk menampung problem atau soal-soal yang ada pada online judge. Database ini bernama problems.tsv yang terdiri dari judul problem dan author problem (berupa username dari author), yang dipisah dengan \t. File otomatis dibuat saat server dijalankan.

C. Client yang telah login, dapat memasukkan command yaitu ‘add’ yang berfungsi untuk menambahkan problem/soal baru pada sistem. Saat client menginputkan command tersebut, server akan meminta beberapa input yaitu:
Judul problem (unique, tidak boleh ada yang sama dengan problem lain)
Path file description.txt pada client (file ini berisi deskripsi atau penjelasan problem)
Path file input.txt pada client (file ini berguna sebagai input testcase untuk menyelesaikan problem)
Path file output.txt pada client (file ini berguna untuk melakukan pengecekan pada submission client terhadap problem)
	Contoh:
	Client-side
add

	
	Server-side
	
Judul problem:
Filepath description.txt:
Filepath input.txt:
Filepath output.txt:


	Client-side
	
<judul-problem-1>
<Client/description.txt-1>
<Client/input.txt-1>
<Client/output.txt-1>


Seluruh file akan disimpan oleh server ke dalam folder dengan nama <judul-problem> yang di dalamnya terdapat file description.txt, input.txt dan output.txt. Penambahan problem oleh client juga akan mempengaruhi file problems.tsv.

D. Client yang telah login, dapat memasukkan command ‘see’ yang berguna untuk menampilkan seluruh judul problem yang ada beserta authornya(author merupakan username client yang menambahkan problem tersebut). Format yang akan ditampilkan oleh server adalah sebagai berikut:

<judul-problem-1> by <author1>
<judul-problem-2> by <author2>


E. Client yang telah login, dapat memasukkan command ‘download <judul-problem>’ yang berguna untuk mendownload file description.txt dan input.txt yang berada pada folder pada server dengan nama yang sesuai dengan argumen kedua pada command yaitu <judul-problem>. Kedua file tersebut akan disimpan ke folder dengan nama <judul-problem> di client.

F. Client yang telah login, dapat memasukkan command ‘submit <judul-problem> <path-file-output.txt>’.  Command ini berguna untuk melakukan submit jawaban dari client terhadap problem tertentu. Algoritma yang dijalankan adalah client akan mengirimkan file output.txt nya melalui argumen ke 3 pada command, lalu server akan menerima dan membandingkan isi file output.txt yang telah dikirimkan oleh client dan output.txt yang ada pada folder dengan nama yang sesuai dengan argumen ke 2 pada command. Jika file yang dibandingkan sama, maka server akan mengirimkan pesan “AC” dan jika tidak maka server akan mengeluarkan pesan “WA”.

G. Server dapat menangani multiple-connection. Dimana jika terdapat 2 atau lebih client yang terhubung ke server, maka harus menunggu sampai client pertama keluar untuk bisa melakukan login dan mengakses aplikasinya.


	Note : 
Dilarang menggunakan system() dan execv(). Silahkan dikerjakan sepenuhnya dengan thread dan socket programming. 
Untuk download dan upload silahkan menggunakan file teks dengan ekstensi dan isi bebas (yang ada isinya bukan touch saja dan tidak kosong)
Untuk error handling jika tidak diminta di soal tidak perlu diberi.

Struktur Direktori Client Server :
├── Client
	├── client.c
└── Server
	├── server.c



3. Nami adalah seorang pengoleksi harta karun handal. Karena Nami memiliki waktu luang, Nami pun mencoba merapikan harta karun yang dimilikinya berdasarkan jenis/tipe/kategori/ekstensi harta karunnya. Setelah harta karunnya berhasil dikategorikan, Nami pun mengirimkan harta karun tersebut ke kampung halamannya.
Contoh jika program pengkategorian dijalankan

Program soal3 terletak di /home/[user]/shift3/hartakarun
./soal3 
Hasilnya adalah sebagai berikut
/home/[user]/shift3/hartakarun
|-jpg
|	--file1.jpg
|	--file2.jpg
|	--file3.jpg
|-c
|	--file1.c
|-tar.gz
|	--file1.tar.gz




A. Hal pertama yang perlu dilakukan oleh Nami adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift3/”. Kemudian working directory program akan berada pada folder “/home/[user]/shift3/hartakarun/”. Karena Nami tidak ingin ada file yang tertinggal, program harus mengkategorikan seluruh file pada working directory secara rekursif

B. Semua file harus berada di dalam folder, jika terdapat file yang tidak memiliki ekstensi, file disimpan dalam folder “Unknown”. Jika file hidden, masuk folder “Hidden”.

C. Agar proses kategori bisa berjalan lebih cepat, setiap 1 file yang dikategorikan dioperasikan oleh 1 thread.

D. Untuk mengirimkan file ke Cocoyasi Village, nami menggunakan program client-server. Saat program client dijalankan, maka folder /home/[user]/shift3/hartakarun/” akan di-zip terlebih dahulu dengan nama “hartakarun.zip” ke working directory dari program client.
Client dapat mengirimkan file “hartakarun.zip” ke server dengan mengirimkan command berikut ke server
send hartakarun.zip


Karena Nami tidak bisa membuat programnya, maka Nami meminta bantuanmu untuk membuat programnya. Bantulah Nami agar programnya dapat berjalan!

Catatan:
Kategori folder tidak dibuat secara manual, harus melalui program C
Program ini tidak case sensitive. Contoh: JPG dan jpg adalah sama
Jika ekstensi lebih dari satu (contoh “.tar.gz”) maka akan masuk ke folder dengan titik terdepan (contoh “tar.gz”)
Dilarang juga menggunakan fork, exec dan system(), kecuali untuk bagian zip pada soal d
Struktur Direktori Client Server :
├── Client
	├── client.c
	└── hartakarun.zip
└── Server
	├── server.c
	└── hartakarun.zip


	                                                        LAPORAN

**Untuk seluruh nomor 1 ini adalah revisi**                                                                
1. Dalam soal ini kami diminta untuk membuat program pemecahan kode-kode dalam file yang disimpan di drive menggunakan deocding base 64. disarankan menggunakan thread.

a. Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan nama quote untuk file zip quote.zip dan music untuk file zip music.zip. Unzip ini dilakukan dengan bersamaan menggunakan thread.

untuk menggunakan thread, kami akan memasukkan library:
    
    #include <pthread.h>

setelah itu kami menggunakan posix thread untuk mengontrol thread yang ada:
    
    pid_t ID;
    pthread_t ThreadID[2];

Selanjutnya ada beberapa fungsi dalam kode yang telah dibuat untuk melakukan unzip ini, dalam kode kami terdaftar:
   
    void unz_thread() //Sebagai fungsi unzip utama

    void *unzip() //Sebagai fungsi untuk eksekusi unzip

    void createFolder() //Sebagai fungsi untuk membuat folder 

Dalam bagian a pada nomor 1 ini berfokus pada unzip, sehingga kami akan menjelaskan bagian fungsi unzip itu sendiri.
Sebelumnya telah didownload secara manual folder dalam bentuk zip, setelah itu akan di unzip menggunakan fungsi diatas.
isi dari fungsi tersebut adalah seperti ini:

    char *unz_music[] = {"unzip", "-q", "music.zip", NULL};
    char *unz_quote[] = {"unzip", "-q", "quote.zip", NULL};
    if(pthread_equal(thread_ID, ThreadID[0]))
    {
        ID = fork();
        if(ID == 0)
            execv("/usr/bin/unzip", unz_music);
    } else if(pthread_equal(thread_ID, ThreadID[1]))
    {
        ID = fork();
        if(ID == 0)
            execv("/usr/bin/unzip", unz_quote);
    }

Fungsi tersebut bertujuan untuk mencari folder bernama music.zip dan quote.zip yang nantinya akan dilocate lalu di unzip dengan memanfaatkan thread & execv.
execv akan berjalan sesuai dengan pememanggilan fungsi fork yang tertulis sebagai **ID = fork();**.
    ID = fork();
    if(ID == 0)
        execv("/usr/bin/unzip", unz_music);

Berikut adalah screenshot hasil dari unzip:

	Sebelum:
![Zipped](https://user-images.githubusercontent.com/90243605/163721898-329cfb48-95f9-497d-b530-ce2cb4f38da7.PNG)

	Sesudah:
![Unzipped](https://user-images.githubusercontent.com/90243605/163729556-871900db-f813-4f40-8b54-142cc04770c6.PNG)

	
    Kesulitan yang dialami:
	1. Terdapat kesulitan pada proses pengalokasian file yang telah di unzip kedalam folder yang telah dibuat
    2. VM error saat ingin melakukan pengambilan gambar hasil unzip
