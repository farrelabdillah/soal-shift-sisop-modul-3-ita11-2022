#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <wait.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <fcntl.h>

pid_t ID;
pthread_t ThreadID[2];

void unz_thread()
{
    pthread_t thread_ID = pthread_self();
    int confirmation = 1;
    
    if (confirmation == 1)
    {
        char *unz_music[] = {"unzip", "-q", "music.zip", NULL};
        char *unz_quote[] = {"unzip", "-q", "quote.zip", NULL};
        if(pthread_equal(thread_ID, ThreadID[0]))
        {
            ID = fork();
            if(ID == 0)
                execv("/usr/bin/unzip", unz_music);
        } else if(pthread_equal(thread_ID, ThreadID[1]))
        {
            ID = fork();
            if(ID == 0)
                execv("/usr/bin/unzip", unz_quote);
        }
    } else
    {
        printf ("cannot unzip");
    }
}

void *unzip()
{
    int confirmation = 1;

    if (confirmation == 1)
    {
        unz_thread();
    } else
    {
        printf("cannot unzip");
    } return NULL;   
}

void createFolder()
{
    char makedirectory[] = "/usr/bin/mkdir";
    char unzipping[] = "/usr/bin/unzip";
    int indicator;
    int start = 1;

    if (start == 1)
    {
        ID = fork();
        char *fold_music[] = {"mkdir", "-p", "/home/farrel/music", NULL};
        char *fold_quote[] = {"mkdir", "-p", "/home/farrel/quote", NULL};
        if(ID == 0)
        {
            execv(makedirectory, fold_music);
        }
        else
        {
            while(wait(&indicator) > 0);
            execv(makedirectory, fold_quote);
        }
    } else
    {
        printf("cannot create folder");
    }
}

int main()
{
    int thread_ind = 1;
    int error_ind;
    int n = 0;
    
    while(n < 2)
    {
        error_ind = pthread_create(&(ThreadID[n]), NULL, &unzip, NULL);
        n++;
    }

    createFolder();

    if (thread_ind == 1)
    {
        pthread_join(ThreadID[0], NULL);
        pthread_join(ThreadID[1], NULL);
    }

    exit(0);

    return 0;
}
